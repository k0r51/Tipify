import HomePage from './components/Pages/HomePage.vue';
import AboutPage from './components/Pages/AboutPage.vue';
import TypingPage from './components/Pages/TypingPage.vue';

const routes = [
  { path: '/', component: HomePage },
  { path: '/typing', component: TypingPage },
  { path: '/about', component: AboutPage },
];

export { routes };
